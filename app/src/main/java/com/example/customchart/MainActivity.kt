package com.example.customchart

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.avengers.customchart.ChartAttribute
import com.avengers.customchart.ChartValue
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        test()

        radarChart.invalidate()
    }

    private fun test() {
        val titles = arrayOf(
            "体重",
            "トップバスト",
            "アンダー バスト",
            "ウエスト",
            "ヒップ",
            "太もも(右)",
            "太もも(左)",
            "バスト高",
            "ウエスト高",
            "ヒップ高"
        )
        val minValues = arrayOf(25f, 60f, 50f, 40f, 60f, 30f, 30f, 80f, 60f, 55f)
        val maxValues = arrayOf(130f, 150f, 125f, 150f, 150f, 90f, 90f, 145f, 120f, 105f)
        val defaultValues = arrayOf(78f, 105f, 88f, 95f, 105f, 60f, 60f, 110f, 90f, 78f)
//        val defaultValues = arrayOf(130f, 150f, 125f, 150f, 150f, 90f, 90f, 145f, 120f, 105f)

        for (i in minValues.indices) {
            object : ChartAttribute<Float> {
                override fun name() = titles[i]
                override fun minValue() = minValues[i]
                override fun maxValue() = maxValues[i]
                override fun valueRange() = maxValues[i] - minValues[i]
            }.let {
                radarChart.addAttribute(it)

                radarChart.putValue(object : ChartValue<Float> {
                    override fun attributeType() = it
                    override fun value() = defaultValues[i]
                    override fun group() = 1

                    override fun hashCode(): Int {
                        return group()
                    }

                    override fun equals(other: Any?): Boolean {
                        if (other == null || other !is ChartValue<*>) return false
                        return other.attributeType() == attributeType()
                    }
                })

                radarChart.putValue(object : ChartValue<Float> {
                    override fun attributeType() = it
                    override fun value() = defaultValues[i] + 20
                    override fun group() = 3

                    override fun hashCode(): Int {
                        return group()
                    }

                    override fun equals(other: Any?): Boolean {
                        if (other == null || other !is ChartValue<*>) return false
                        return other.attributeType() == attributeType()
                    }
                })

                radarChart.putValue(object : ChartValue<Float> {
                    override fun attributeType() = it
                    override fun value() = defaultValues[i] + 100
                    override fun group() = 3

                    override fun hashCode(): Int {
                        return group()
                    }

                    override fun equals(other: Any?): Boolean {
                        if (other == null || other !is ChartValue<*>) return false
                        return other.attributeType() == attributeType()
                    }
                })
            }
        }
    }

}
