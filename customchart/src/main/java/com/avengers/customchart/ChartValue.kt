package com.avengers.customchart

interface ChartValue<out T : Number> {
    fun attributeType(): ChartAttribute<T>
    fun value(): T
    fun group(): Int
}