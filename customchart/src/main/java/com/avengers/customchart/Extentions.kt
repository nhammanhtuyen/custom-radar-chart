package com.avengers.customchart

import android.graphics.PointF
import kotlin.math.cos
import kotlin.math.sin


fun PointF.translateByDegees(degrees: Double, distance: Float) =
    translateByRadian(Math.toRadians(degrees), distance)

fun PointF.translateByRadian(radians: Double, distance: Float): PointF {
    val cos = cos(radians).toFloat()
    val sin = sin(radians).toFloat()

    return PointF(cos * distance + x, sin * distance + y)
}

fun PointF.rotateByRadian(
    centerX: Float,
    centerY: Float,
    radians: Double
): PointF {
    val deltaX = x - centerX
    val deltaY = y - centerY
    val cos = cos(radians).toFloat()
    val sin = sin(radians).toFloat()

    set(deltaX * cos - deltaY * sin + centerX, deltaX * sin + deltaY * cos + centerY)
    return this
}

fun PointF.rotateByDegees(
    centerX: Float,
    centerY: Float,
    degees: Double
) = rotateByRadian(centerX, centerY, Math.toRadians(degees))


