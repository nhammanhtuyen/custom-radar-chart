package com.avengers.customchart

interface ChartAttribute<out T : Number> {

    fun name(): String

    fun minValue(): T

    fun maxValue(): T

    fun valueRange(): T // maxValue - minValue
}