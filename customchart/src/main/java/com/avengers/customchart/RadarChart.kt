package com.avengers.customchart

import android.content.Context
import android.graphics.*
import android.graphics.Paint.FILTER_BITMAP_FLAG
import android.util.AttributeSet
import android.view.View
import kotlin.math.pow
import kotlin.math.sqrt


class RadarChart(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    private val circlePaint: Paint
    private val linePaint: Paint
    private val textPaint: Paint
    private val titlePaint: Paint
    private val pathPaint: Paint
    private val pointPaint: Paint

    private var maxRadius: Float = 0f
    private var maxTitleWidth: Float = 0f
    private var centerX: Float = 0f
    private var centerY: Float = 0f
    private var paddingText: Float = 8f
    private var divisionCount: Int
    private var valueColor: Int
    private var titleColor: Int
    private var lineColor: Int
    private var radarCircleColor: Int
    private var maxValueTextSize: Float
    private var maxTitleTextSize: Float
    private var lineStrokeWidth: Float
    private var circleStrokeWidth: Float
    private var pathStrokeWidth: Float
    private var displayValuePoint: Boolean
    private var displayValuePath: Boolean
    private var valuePointRadius: Float
    private var valuePointColor: Int

    private val chartAttributes = mutableSetOf<ChartAttribute<Number>>()
    private val chartValues = mutableSetOf<ChartValue<Number>>()

    init {
        val typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RadarChart)

        with(typedArray) {
            radarCircleColor = getColor(R.styleable.RadarChart_circleColor, Color.BLACK)
            lineColor = getColor(R.styleable.RadarChart_axesColor, Color.BLACK)
            titleColor = getColor(R.styleable.RadarChart_titleColor, Color.BLACK)
            valueColor = getColor(R.styleable.RadarChart_valueColor, Color.BLACK)
            valuePointColor = getColor(R.styleable.RadarChart_valuePointColor, Color.BLACK)
            maxTitleTextSize = getDimension(R.styleable.RadarChart_maxTitleTextSize, 32f)
            maxValueTextSize = getDimension(R.styleable.RadarChart_maxValueTextSize, 24f)
            divisionCount = getInt(R.styleable.RadarChart_divisionCount, 5)
            lineStrokeWidth = getDimension(R.styleable.RadarChart_axesStrokeWidth, 1f)
            circleStrokeWidth = getDimension(R.styleable.RadarChart_circleStrokeWidth, 1f)
            pathStrokeWidth = getDimension(R.styleable.RadarChart_pathStrokeWidth, 1f)
            valuePointRadius = getDimension(R.styleable.RadarChart_valuePointRadius, 4f)
            displayValuePoint = getBoolean(R.styleable.RadarChart_displayValuePoint, true)
            displayValuePath = getBoolean(R.styleable.RadarChart_displayValuePath, true)
            recycle()
        }

        reset()

        circlePaint = getDefaultPaint().apply {
            strokeWidth = circleStrokeWidth
            style = Paint.Style.STROKE
            color = radarCircleColor
        }

        linePaint = getDefaultPaint().apply {
            strokeWidth = lineStrokeWidth
            style = Paint.Style.STROKE
            color = lineColor
        }

        textPaint = getDefaultPaint().apply {
            style = Paint.Style.FILL
            textSize = maxValueTextSize
            textAlign = Paint.Align.CENTER
            color = valueColor
        }

        titlePaint = getDefaultPaint().apply {
            style = Paint.Style.FILL
            textSize = maxTitleTextSize
            color = titleColor
            textAlign = Paint.Align.CENTER
        }

        pathPaint = getDefaultPaint().apply {
            flags = FILTER_BITMAP_FLAG
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeWidth = pathStrokeWidth
        }

        pointPaint = getDefaultPaint().apply {
            style = Paint.Style.FILL
            color = valuePointColor
        }

        test()
    }

    private fun test() {
        val titles = arrayOf(
            "体重",
            "トップバスト",
            "アンダー バスト",
            "ウエスト",
            "ヒップ",
            "太もも(右)",
            "太もも(左)",
            "バスト高",
            "ウエスト高",
            "ヒップ高"
        )
        val minValues = arrayOf(25f, 60f, 50f, 40f, 60f, 30f, 30f, 80f, 60f, 55f)
        val maxValues = arrayOf(130f, 150f, 125f, 150f, 150f, 90f, 90f, 145f, 120f, 105f)
        val defaultValues = arrayOf(78f, 105f, 88f, 95f, 105f, 60f, 60f, 110f, 90f, 78f)
//        val defaultValues = arrayOf(130f, 150f, 125f, 150f, 150f, 90f, 90f, 145f, 120f, 105f)

        for (i in minValues.indices) {
            object : ChartAttribute<Float> {
                override fun name() = titles[i]
                override fun minValue() = minValues[i]
                override fun maxValue() = maxValues[i]
                override fun valueRange() = maxValues[i] - minValues[i]
            }.let {
                addAttribute(it)

                putValue(object : ChartValue<Float> {
                    override fun attributeType() = it
                    override fun value() = defaultValues[i]
                    override fun group() = 1

                    override fun hashCode(): Int {
                        return group()
                    }

                    override fun equals(other: Any?): Boolean {
                        if (other == null || other !is ChartValue<*>) return false
                        return other.attributeType() == attributeType()
                    }
                })

                putValue(object : ChartValue<Float> {
                    override fun attributeType() = it
                    override fun value() = defaultValues[i] + 20
                    override fun group() = 3

                    override fun hashCode(): Int {
                        return group()
                    }

                    override fun equals(other: Any?): Boolean {
                        if (other == null || other !is ChartValue<*>) return false
                        return other.attributeType() == attributeType()
                    }
                })

                putValue(object : ChartValue<Float> {
                    override fun attributeType() = it
                    override fun value() = defaultValues[i] + 100
                    override fun group() = 3

                    override fun hashCode(): Int {
                        return group()
                    }

                    override fun equals(other: Any?): Boolean {
                        if (other == null || other !is ChartValue<*>) return false
                        return other.attributeType() == attributeType()
                    }
                })
            }
        }
    }

    companion object {
        fun getDefaultPaint() = Paint().apply {
            flags = Paint.ANTI_ALIAS_FLAG
            isAntiAlias = true
            strokeWidth = 1f
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        centerX = width.div(2).toFloat()
        centerY = height.div(2).toFloat()

        maxTitleWidth = minOf(centerX, centerY) / 3
        maxRadius = minOf(centerX, centerY) - maxTitleWidth - getMaxValueTextWidth()

        titlePaint.apply {
            textSize =
                calculateTextSizeForWidth(
                    this,
                    maxTitleWidth,
                    calculateLargestTitleWidth()
                ).coerceAtMost(maxTitleTextSize)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        for (i in 1..divisionCount)
            canvas.drawCircle(
                centerX,
                centerY,
                maxRadius.div(divisionCount).times(i),
                circlePaint
            )

        if (chartAttributes.size <= 0) return

        chartAttributes.forEachIndexed { index, chartAttribute ->
            drawAxes(canvas, index)

            rotateAndDrawText(
                chartAttribute.name(),
                canvas,
                getAxesAngle(index),
                centerX,
                centerY,
                maxRadius + maxTitleWidth.div(2) + getMaxValueTextWidth(),
                titlePaint
            )

            drawDivisonValues(canvas, chartAttribute, getAxesAngle(index))
        }

        if (chartValues.size <= 0) return

        chartValues.groupBy { it.group() }.values.forEach { values ->
            val points = calculateValuePoints(values)
            if (displayValuePath)
                drawValuePaths(canvas, points, pathPaint)
            if (displayValuePoint)
                drawValuePoints(canvas, points, pointPaint)
        }
    }

    private fun getMaxValueTextWidth() = getTextWidth(
        calculateLargestValueTextWidth(),
        textPaint
    )

    private fun calculateLargestTitleWidth() =
        chartAttributes.maxBy { getTextWidth(it.name(), titlePaint) }?.name() ?: ""

    private fun calculateLargestValueTextWidth() =
        chartAttributes.maxBy { getTextWidth(it.maxValue().toString(), textPaint) }?.maxValue()
            .toString()

    private fun getAxesAngle(index: Int) = if (chartAttributes.size <= 0)
        0.0
    else
        360.0.div(chartAttributes.size).times(index)

    private fun calculateValuePoints(chartValues: List<ChartValue<Number>>): MutableList<PointF> {
        val points = mutableListOf<PointF>()

        chartAttributes.forEachIndexed lit@{ index, chartAttribute ->
            val maxValue = chartAttribute.maxValue().toFloat()
            val minValue = chartAttribute.minValue().toFloat()
            val value =
                chartValues.find { it.attributeType() == chartAttribute }?.value()
                    ?.toFloat()
                    ?.coerceAtMost(maxValue)
                    ?.coerceAtLeast(minValue)
                    ?: return@lit
            val valuePosition = PointF().apply {
                set(
                    centerX,
                    centerY - value.minus(minValue)
                        .div(chartAttribute.valueRange().toFloat())
                        .times(maxRadius)
                )

                rotateByDegees(centerX, centerY, getAxesAngle(index))
            }

            points.add(valuePosition)
        }

        return points
    }

    private fun drawValuePoints(canvas: Canvas, points: List<PointF>, paint: Paint) {
        points.forEach {
            canvas.drawCircle(it.x, it.y, valuePointRadius, paint)
        }
    }

    private fun drawValuePaths(canvas: Canvas, points: List<PointF>, paint: Paint) {
        val path = Path()
        val iterator = points.iterator()
        var lastPoint = points.first()
        with(path) {
            while (iterator.hasNext()) {
                lastPoint = iterator.next().let {
                    paint.shader = LinearGradient(
                        lastPoint.x,
                        lastPoint.y,
                        it.x, it.y,
                        Color.BLUE,
                        Color.RED,
                        Shader.TileMode.REPEAT
                    )

                    if (isEmpty)
                        moveTo(it.x, it.y)
                    else
                        lineTo(it.x, it.y)

                    it
                }
            }

            close()
        }

        canvas.drawPath(path, paint)
    }

    private fun drawAxes(
        canvas: Canvas,
        index: Int
    ) {
        canvas.save()
        canvas.rotate(getAxesAngle(index).toFloat(), centerX, centerY)
        canvas.drawLine(
            centerX,
            centerY,
            centerX,
            centerY - maxRadius,
            linePaint
        )
        canvas.restore()
    }

    private fun drawDivisonValues(
        canvas: Canvas,
        chartAttribute: ChartAttribute<*>,
        degees: Double
    ) {
        for (j in 1..divisionCount) {
            val maxValue = chartAttribute.maxValue().toFloat()
            val minValue = chartAttribute.minValue().toFloat()
            val valuePerDivision = maxValue.minus(minValue).div(divisionCount)
            val divisionValue = valuePerDivision.times(j).plus(minValue).toInt()
            val textBounds = getTextBounds(divisionValue.toString(), textPaint)

            rotateAndDrawText(
                divisionValue.toString(),
                canvas,
                degees,
                centerX,
                centerY,
                j * maxRadius / divisionCount,
                textPaint
            ) {
                //translate 45 degrees
                it.translateByDegees(
                    degees - 45.0,
                    sqrt(textBounds.exactCenterX().pow(2) + textBounds.exactCenterY().pow(2))
                        .plus(paddingText)
                )
            }
        }
    }

    private fun rotateAndDrawText(
        text: String,
        canvas: Canvas,
        degees: Double,
        centerX: Float,
        centerY: Float,
        distance: Float,
        paint: Paint,
        translate: ((PointF) -> PointF)? = null
    ) {
        val textBound = getTextBounds(text, paint)

        val rotatedPositon =
            PointF(
                centerX,
                centerY - distance
            ).rotateByDegees(
                centerX,
                centerY,
                degees
            )

        val textPosition = translate?.invoke(rotatedPositon) ?: rotatedPositon

        canvas.drawText(
            text,
            textPosition.x,
            textPosition.y + (textBound.height() / 2).toFloat(),
            paint
        )
    }

    private fun calculateTextSizeForWidth(
        paint: Paint, desiredWidth: Float,
        text: String
    ) = paint.textSize * desiredWidth / getTextWidth(text, paint)

    private fun getTextWidth(text: String, paint: Paint) = getTextBounds(text, paint).width()

    private fun getTextBounds(text: String, paint: Paint) = Rect().apply {
        paint.getTextBounds(text, 0, text.length, this)
    }

    fun <T : Number> addAttribute(chartAttribute: ChartAttribute<T>) =
        chartAttributes.add(chartAttribute)

    fun <T : Number> putValue(value: ChartValue<T>) = chartValues.add(value)

    fun reset() {
        chartAttributes.clear()
        chartValues.clear()

        invalidate()
    }
}